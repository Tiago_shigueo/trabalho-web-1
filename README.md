# Trabalho final Web 1

# Equipe:

- Carolina dos Santos de Lima
- Isabelle dos Santos Cassiano
- Tiago Shigueo Yajima Yamasaki

# Funcionamento da página

A página inicial é a página de catálogo, onde o cliente poderá ver quais filmes estão disponíveis para alugar. Caso o cliente deseje alugar um filme, basta clicar no botão "Alugar filme" para registrar a locação.
Na tela para alugar filmes, basta o cliente preencher com o nome, e-mail, telefone, data de retirada, data de entrega/devolução e o nome do filme que deseja alugar.
Já a "Área do administrador" é para editar, deletar ou adicionar novos filmes no catálogo da locadora.
