<?php
    require_once "../credentials.php";
    $conn = mysqli_connect($servername, $username, $password);
  $sql = "CREATE DATABASE $dbname";
    if (mysqli_query($conn, $sql)) {
        echo "Database created successfully";
    }
    else
    {
        echo "error creating database: " . mysqli_error($conn);
    }
    mysqli_close($conn);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></script>
    <link rel="stylesheet" href="style.css">
    <title>Página inicial</title>
</head>
<body>
    <div class="navbar">
        <a href="register.php" id="register">Alugar filme</a>
        <a href="about_us.php" id="about_us">Sobre nós</a>
        <a href="dashboard.php" id="dashboard">Área do administrador</a>

    </div>
    <div class="catalogos">
        <div id="catalogo-1">
           <img src="../Images/sonic2.jfif" alt="">
           <h3>Sonic 2</h3>
           <p>Data de lançamento: 07/04/2022</p>
           <p>Duração do filme: 2h 2m</p>  
           <p>Valor: R$ 15,00</p>   
        </div>
        <div id="catalogo-2">
          <img src="../Images/encantado.jfif" alt="">
           <h3>Encantado</h3>
           <p>Data de lançamento: 29/11/2018</p>
           <p>Duração do filme: 1h 25m</p>  
           <p>Valor: R$ 11,90</p>   
        </div>
        <div id="catalogo-3">
           <img src="../Images/avatar.jfif" alt="">
           <h3>Avatar</h3>
           <p>Data de lançamento: 15/12/2022</p>
           <p>Duração do filme: 1h 5m</p>  
           <p>Valor: R$ 13,00</p>   
        </div>
    </div>
</body>
</html>
</body>
</html>