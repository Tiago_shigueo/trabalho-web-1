<?php
//{
//include_once('../credentials.php');

//$nome = $_POST['nome'];
//$email = $_POST['email'];
//$telefone = $_POST['telefone'];
//$sexo = $_POST['sexo'];
//$data_nasc = $_POST['data_nasc'];
//$cidade = $_POST['cidade'];
//$estado = $_POST['estado'];
//$endereco = $_POST['endereco'];
//$senha = $_POST['senha'];
// Deletar daqui para baixo
//$DataLoca = $_POST['DataLoca'];
//$DataDevo = $_POST['DataDevo'];
//$nome_filme = $_POST['nome_filme'];
//$data_entrega_real = $_POST['data_entrega_real'];

//$result = mysqli_query($conexao, "INSERT INTO usario(nome,email,telefone,sexo,data_nasc,cidade,estado,endereco,senha) 
//        VALUES ('$nome','$email','$telefone','$sexo','$data_nasc','$cidade','$estado,'$endereco','$senha')");
//}

function verifica_campo($texto){
    $texto = trim($texto); // Remove os espaços em branco
    $texto = stripslashes($texto); // 
    $texto = htmlspecialchars($texto);
    return $texto; 
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <script src="verificador.js"></script>
    <title>Cadastro</title>
</head>
<body>
    <div class="navbar">
        <a href="index.php" id="home">Página inicial</a>
        <a href="about_us.php" id="about_us">Sobre nós</a>
    </div>
    
    <div class="box">
        <form action="index.php" method="POST">
            <fieldset>
                <legend><b> Cadastro de cliente e título</b></legend>
                <br>
                <div class="inputBox">
                    <input type="text" name="nome" id="nome" class="inputUser" required>
                    <label for="nome" class="labelInput">Nome completo</label>
                </div>
                <br><br>
                <div class="inputBox">
                    <input type="text" name="email" id="email" class="inputUser" required>
                    <label for="email" class="labelInput">Email</label>
                </div>
                <br><br>
                <div class="inputBox">
                    <input type="tel" name="telefone" id="telefone" class="inputUser" required>
                    <label for="telefone" class="labelInput">Telefone</label>
                </div>
                <br><br>
                <label for="data_retirada"><b>Data de retirada:</b></label>
                <input type="date" name="data_retirada" id="data_retirada" required>
                <br><br>
                <label for="data_entrega"><b>Data de entrega:</b></label>
                <input type="date" name="data_entrega" id="data_entrega" required>
                <br><br><br>
                <div class="inputBox">
                    <input type="text" name="titulo_video" id="titulo_video" class="inputUser" required>
                    <label for="titulo_video" class="labelInput">Título do video</label>
                </div>
                <br><br>
                <input type="submit" name="submit" id="submit">
            </fieldset>
        </form>
    </div>
</body>
</html>