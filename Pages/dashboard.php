<?php
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <title>Área do admin</title>
</head>
<body>
    <div class="navbar">
        <a href="index.php" id="home">Página inicial</a>
        <a href="register.php" id="register">Registro</a>
        <a href="about_us.php" id="about_us">Sobre nós</a>
    </div>
    <div class="dashboard_body">
    <h1>Área do administrador</h1>
            <button><a href="dash_register.php">Cadastrar novo filme</a></button>
            <button><a href="dash_delete.php">Apagar um filme</a></button>

        <table class="dash_table">
            <tr>
                <th>Nomes dos filmes</th>
                <th>Gênero</th>
                
            </tr>
            <tr>
                <td>Sonic 2</td>
                <td>Lorem Ipsum</td>
            </tr>
            <tr>
                <td>Encantados</td>
                <td>Lorem Ipsum</td>
            </tr>
            <tr>
                <td>Avatar 2</td>
                <td>Lorem Ipsum</td>
            </tr>
        </table>
    </div>
</body>
</html>
<!-- style="width:100%" border="1px"-->